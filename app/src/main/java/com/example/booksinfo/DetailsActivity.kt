package com.example.booksinfo

import android.content.Context
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.booksinfo.data.Book
import java.lang.StringBuilder


class DetailsActivity : AppCompatActivity(){

    private lateinit var book: Book
    private lateinit var title: TextView
    private lateinit var subtitle: TextView
    private lateinit var authors: TextView
    private lateinit var publisher: TextView
    private lateinit var publishDate: TextView
    private lateinit var categories: TextView
    private lateinit var overviewTitle: TextView
    private lateinit var overview: TextView

    // saleInfo (country, saleability, listPrice, retailPrice, buyLink and offers) details.

    private lateinit var saleInfo: TextView
    private lateinit var country: TextView
    private lateinit var saleability: TextView
    private lateinit var listPrice: TextView
    private lateinit var retailPrice: TextView
    private lateinit var buyLink: TextView
    private lateinit var offers: TextView
    lateinit var poster: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        poster = findViewById(R.id.poster)
        title = findViewById(R.id.title)
        subtitle = findViewById(R.id.subtitle)
        authors = findViewById(R.id.authors)
        publisher = findViewById(R.id.publisher)
        publishDate = findViewById(R.id.publishdate)
        categories = findViewById(R.id.categories)
        overviewTitle = findViewById(R.id.overviewTitle)
        overview = findViewById(R.id.overview)
        saleInfo = findViewById(R.id.saleinfo)
        country = findViewById(R.id.country)
        saleability = findViewById(R.id.salebility)
        listPrice = findViewById(R.id.listprice)
        retailPrice = findViewById(R.id.retailprice)
        buyLink= findViewById(R.id.buylink)
        offers = findViewById(R.id.offers)

        val mContext: Context = this@DetailsActivity
        val bundle: Bundle? = intent.extras

        //Intent with data
        book = bundle?.get("bookinfo") as Book

        if(book != null)  {

            Glide.with(mContext)
                .load(book?.volumeInfo?.imageLinks?.thumbnail)
                .into(poster)

            title.text = "Title :" + book?.volumeInfo.title
            subtitle.text ="Subtitle: " + book?.volumeInfo.subtitle
            authors.text = "Authors: " + book?.volumeInfo.authors.toString()
            publisher.text = "Publisher: " + book?.volumeInfo.publisher
            publishDate.text = "PublishDate: " + book?.volumeInfo.publishedDate
            categories.text = "Categories: " + book?.volumeInfo.categories.toString()
            overviewTitle.text = "Description"
            overview.text = book?.volumeInfo.description
            val countrystr: String = "Country: " + book?.saleInfo.country
            country.text = countrystr
            val salebilitystr : String ="Salebility: " + book?.saleInfo.saleability
            saleability.text =  salebilitystr
            val listprc : String ="List " +( (book?.saleInfo?.listPrice?.toString())?:(" price Empty"))
            listPrice.text =  listprc
            val retprc : String ="Retail " + ((book?.saleInfo?.retailPrice?.toString())?:" price Empty")
            retailPrice.text = retprc

            val buystr : String ="BuyLink: " + ((book?.saleInfo.buyLink)?:"Empty")
            buyLink.text = buystr
            var displayOffers:StringBuilder = StringBuilder()
            val check: Boolean = (book?.saleInfo?.offers?.isEmpty())?:true
            if(!check) {
                for( offers in book?.saleInfo!!.offers!!) {
                    displayOffers.append(offers?.toString())
                    displayOffers.append("\n")
                }
                offers.text = "Offers: " + displayOffers.toString()
            } else {
                offers.text = "Offers: None"

            }

        }

    }

}

