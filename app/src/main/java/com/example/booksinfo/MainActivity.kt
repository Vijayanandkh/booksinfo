package com.example.booksinfo

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.booksinfo.data.Book
import com.example.booksinfo.ui.main.BooksAdapter
import com.example.booksinfo.ui.main.MainViewModel

class MainActivity : AppCompatActivity(), BooksAdapter.BookClickListener {

    private lateinit var viewModel: MainViewModel

    lateinit var recyclerViewBooks : RecyclerView
    lateinit var booksAdapter: BooksAdapter
    var listener: BooksAdapter.BookClickListener = this
    var bookList: List<Book>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        recyclerViewBooks = findViewById(R.id.recyclerViewBooks);
        recyclerViewBooks.setHasFixedSize(true);
        recyclerViewBooks.setLayoutManager(LinearLayoutManager(this));

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)



        viewModel.getBooks()?.observe(this, object : Observer<List<Book>> {
            override fun onChanged(t: List<Book>?) {
                bookList = t
                booksAdapter = BooksAdapter(this@MainActivity, t, listener)

                recyclerViewBooks.adapter = booksAdapter
            }

        })
    }


    override fun onBookSelectButtonClicked(id: Int) {
        Log.d("BOOK", "book index is: " + id)
        val intent = Intent(this@MainActivity, DetailsActivity::class.java)
        var bookInfo: Book
        bookInfo = bookList!![id]
        intent.putExtra("bookinfo", bookInfo)
        startActivity(intent)

    }
}

