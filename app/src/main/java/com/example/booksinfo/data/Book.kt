package com.example.booksinfo.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//	Each book in the list should display its title, publisher and smallThumbnail image.
//	Upon clicking a book, reveal its volumeInfo (title, subtitle, thumbnail, authors, publisher, publishedDate, description and categories) and saleInfo (country, saleability, listPrice, retailPrice, buyLink and offers) details.

@Parcelize
data class Book(

    @SerializedName("id") var id : String,
    @SerializedName("kind") var kind : String,
    @SerializedName("etag") var etag: String,
    @SerializedName("volumeInfo") var volumeInfo: VolumeInfo,
    @SerializedName("saleInfo") var saleInfo: SaleInfo

    ) /*
    override fun writeToParcel(dest: Parcel?, flags: Int) {
\

    }*/ : Parcelable
