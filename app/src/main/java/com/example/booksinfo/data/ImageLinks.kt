package com.example.booksinfo.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageLinks(
    @SerializedName("smallThumbnail") var smallThumbnail: String,
    @SerializedName("thumbnail") var thumbnail: String
    ) : Parcelable