package com.example.booksinfo.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListPrice(
    @SerializedName("amount") var amount: Double,
    @SerializedName("currencyCode") var currencyCode: String
    ) : Parcelable {

    override fun toString(): String {
         val msg: String =("Price(amount=$amount, currencyCode='$currencyCode')")?:"Price not found"
        return msg
    }
}