package com.example.booksinfo.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Offers(
    @SerializedName("finskyOfferType") var finskyOfferType: Int,
    @SerializedName("listPrice") var listPrice: ListPrice,
    @SerializedName("retailPrice") var retailPrice: ListPrice,
    @SerializedName("giftable") var giftable: Boolean
) : Parcelable {
    override fun toString(): String {
        return "Offers(finskyOfferType=$finskyOfferType, listPrice=$listPrice, retailPrice=$retailPrice, giftable=$giftable)"
    }
}