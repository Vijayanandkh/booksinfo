package com.example.booksinfo.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Results (

    @SerializedName("kind") var kind: String,
    @SerializedName("totalItems") var totalItems: Int,

    @SerializedName("items") var items: List<Book>,
    @SerializedName("page") var page: Int

    ) : Parcelable