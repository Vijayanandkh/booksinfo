package com.example.booksinfo.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// saleInfo (country, saleability, listPrice, retailPrice, buyLink and offers)
@Parcelize
data class SaleInfo(
    @SerializedName("country") var country: String?,
    @SerializedName("saleability") var saleability: String?,
    @SerializedName("isEbook") var isEbook: Boolean,
    @SerializedName("listPrice") var listPrice: ListPrice?,
    @SerializedName("retailPrice") var retailPrice: ListPrice?,
    @SerializedName("buyLink") var buyLink: String?,
    @SerializedName("offers") var offers: List<Offers>?
    ) : Parcelable