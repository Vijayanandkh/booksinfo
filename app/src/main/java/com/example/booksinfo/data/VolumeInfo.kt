package com.example.booksinfo.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


//volumeInfo

//	Upon clicking a book reveal its
//	volumeInfo
//  (title, subtitle, thumbnail, authors, publisher, publishedDate, description and categories)
// and saleInfo (country, saleability, listPrice, retailPrice, buyLink and offers) details.

@Parcelize
data class VolumeInfo(
    @SerializedName("title") var title: String,
    @SerializedName("subtitle") var subtitle: String?,
    @SerializedName("imageLinks") var imageLinks: ImageLinks?,
    @SerializedName("authors") var  authors: List<String>?,
    @SerializedName("publisher") var publisher: String?,
    @SerializedName("publishedDate") var publishedDate: String?,
    @SerializedName("description") var description: String?,
    @SerializedName("categories") var categories: List<String>?
) : Parcelable