package com.example.booksinfo.network

import com.example.booksinfo.data.Results
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface BooksApiInterface {
    @GET("volumes?")
    fun fetchBooks( @QueryMap options: Map<String, String>): Call<Results>


/*
    @GET("movie/popular?")
    fun fetchPopular( @QueryMap options: Map<String, String>): Call<Results>


    @GET("movie/{Id}?")
    fun fetchMovie(
        @Path("Id") movieId: Int,
        @Query("api_key") api_key: String
    ): Call<MovieInfo>*/


}