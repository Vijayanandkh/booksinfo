package com.example.booksinfo.ui.main

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.booksinfo.R
import com.example.booksinfo.data.Book
import com.squareup.picasso.Picasso


class BooksAdapter(var mContext : Context , var books: List<Book?>?, var listener: BookClickListener) : RecyclerView.Adapter<BooksAdapter.BookViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksAdapter.BookViewHolder {
        return BookViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.book_item,
                parent,
                false
            )
            , listener, mContext)
    }

    override fun getItemCount() = books!!.size

    override fun onBindViewHolder(holder: BooksAdapter.BookViewHolder, position: Int) {
        holder.bind(books!!.get(position), listener)
    }

    class BookViewHolder(itemView: View, var listener: BookClickListener, var mContext: Context) : RecyclerView.ViewHolder(itemView) {

        lateinit var poster: ImageView
        lateinit var titleView: TextView
        lateinit var publisherView: TextView



        fun bind(item: Book?, listener: BookClickListener) {

            itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    var book_identity = item?.id
                   // Log.d("MOVIE_ID", " Movie id: " + book_identity)
                    if (book_identity != null) {
                        listener.onBookSelectButtonClicked(adapterPosition)
                    }
                }
            });

            poster = itemView.findViewById(R.id.poster)
            titleView = itemView.findViewById(R.id.title)
            publisherView = itemView.findViewById(R.id.publisher)
            //poster.setImageURI(Uri.parse("https://image.tmdb.org/t/p/original/${item.posterPath}"))

            poster.tag = item?.id

            Glide.with(mContext)
                .load(item?.volumeInfo?.imageLinks?.smallThumbnail)
                .into(poster)

/*
            Picasso.with(itemView.context)
                .load(Uri.parse(item?.volumeInfo?.imageLinks?.smallThumbnail)).into(poster)

*/
            titleView.text = item?.volumeInfo?.title
            publisherView.text = item?.volumeInfo?.publisher

        }
    }


    interface BookClickListener {
        fun onBookSelectButtonClicked(id: Int)
    }
}