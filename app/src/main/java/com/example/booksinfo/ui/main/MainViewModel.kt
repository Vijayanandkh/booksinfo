package com.example.booksinfo.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.booksinfo.data.Book
import com.example.booksinfo.data.Results
import com.example.booksinfo.network.BooksApiInterface
import com.example.booksinfo.network.RetrofitClientInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainViewModel : ViewModel() {

    private var results: Results?= null;

    //this is the data that we will fetch asynchronously
    private var bookList: MutableLiveData<List<Book>>? = null

    //we will call this method to get the data
    fun getBooks(): LiveData<List<Book>>? {
        //if the list is null
        if (bookList == null) {
            bookList = MutableLiveData<List<Book>>()
            //we will load it asynchronously from server in this method
            loadBooks()
        }

        //finally we will return the list
        return bookList
    }


    public fun getRetrofitClient() : BooksApiInterface {
        return RetrofitClientInstance.getRetrofitInstance()!!.create(BooksApiInterface::class.java)
    }
    //This method is using Retrofit to get the JSON data from URL
    private fun loadBooks() {

//        https://www.googleapis.com/books/v1/volumes?q=health&maxResults=20

        val booksApiInterface : BooksApiInterface = getRetrofitClient()
        var dataquerry = HashMap<String, String>()
        dataquerry.put("q", "health")
        dataquerry.put("maxResults", "20")

        val call: Call<Results> = booksApiInterface.fetchBooks(dataquerry)
        call.enqueue(object : Callback<Results> {
            override fun onResponse(
                call: Call<Results>,
                response: Response<Results>
            ) {

                //finally we are setting the list to our MutableLiveData
                bookList!!.setValue(response.body()!!.items)
            }

            override fun onFailure(
                call: Call<Results>?,
                t: Throwable?
            ) {
                Log.d("erorr in response", " data not receive" + t?.message)
            }
        })
    }
}